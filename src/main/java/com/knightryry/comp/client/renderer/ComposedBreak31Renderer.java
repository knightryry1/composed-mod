package com.knightryry.comp.client.renderer;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.client.renderer.entity.layers.HumanoidArmorLayer;
import net.minecraft.client.renderer.entity.HumanoidMobRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.model.geom.ModelLayers;
import net.minecraft.client.model.HumanoidModel;

import com.knightryry.comp.entity.ComposedBreak31Entity;

public class ComposedBreak31Renderer extends HumanoidMobRenderer<ComposedBreak31Entity, HumanoidModel<ComposedBreak31Entity>> {
	public ComposedBreak31Renderer(EntityRendererProvider.Context context) {
		super(context, new HumanoidModel(context.bakeLayer(ModelLayers.PLAYER)), 0.5f);
		this.addLayer(new HumanoidArmorLayer(this, new HumanoidModel(context.bakeLayer(ModelLayers.PLAYER_INNER_ARMOR)), new HumanoidModel(context.bakeLayer(ModelLayers.PLAYER_OUTER_ARMOR))));
	}

	@Override
	public ResourceLocation getTextureLocation(ComposedBreak31Entity entity) {
		return new ResourceLocation("composedbreak:textures/entities/egirl_skin_cow.png");
	}
}
