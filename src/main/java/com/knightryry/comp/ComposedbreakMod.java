/*
 *	MCreator note:
 *
 *	If you lock base mod element files, you can edit this file and the proxy files
 *	and they won't get overwritten. If you change your mod package or modid, you
 *	need to apply these changes to this file MANUALLY.
 *
 *
 *	If you do not lock base mod element files in Workspace settings, this file
 *	will be REGENERATED on each build.
 *
 */
package com.knightryry.comp;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import net.fabricmc.api.ModInitializer;

import com.knightryry.comp.init.ComposedbreakModTabs;
import com.knightryry.comp.init.ComposedbreakModProcedures;
import com.knightryry.comp.init.ComposedbreakModMenus;
import com.knightryry.comp.init.ComposedbreakModItems;
import com.knightryry.comp.init.ComposedbreakModEntities;

public class ComposedbreakMod implements ModInitializer {
	public static final Logger LOGGER = LogManager.getLogger();
	public static final String MODID = "composedbreak";

	@Override
	public void onInitialize() {
		LOGGER.info("Initializing ComposedbreakMod");
		ComposedbreakModTabs.load();

		ComposedbreakModEntities.load();

		ComposedbreakModItems.load();

		ComposedbreakModProcedures.load();

		ComposedbreakModMenus.load();

	}
}
