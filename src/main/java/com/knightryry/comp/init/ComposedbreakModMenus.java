
/*
 *	MCreator note: This file will be REGENERATED on each build.
 */
package com.knightryry.comp.init;

import net.minecraft.world.inventory.MenuType;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.core.Registry;

import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerType;

import com.knightryry.comp.world.inventory.InvMenu;
import com.knightryry.comp.client.gui.InvScreen;
import com.knightryry.comp.ComposedbreakMod;

public class ComposedbreakModMenus {
	public static MenuType<InvMenu> INV;

	public static void load() {
		INV = Registry.register(Registry.MENU, new ResourceLocation(ComposedbreakMod.MODID, "inv"), new ExtendedScreenHandlerType<>(InvMenu::new));
		InvScreen.screenInit();
	}
}
