
/*
 *	MCreator note: This file will be REGENERATED on each build.
 */
package com.knightryry.comp.init;

import net.fabricmc.fabric.api.client.rendering.v1.EntityRendererRegistry;
import net.fabricmc.api.Environment;
import net.fabricmc.api.EnvType;

import com.knightryry.comp.client.renderer.ComposedBreak31Renderer;

@Environment(EnvType.CLIENT)
public class ComposedbreakModEntityRenderers {
	public static void load() {
		EntityRendererRegistry.register(ComposedbreakModEntities.COMPOSED_BREAK_31, ComposedBreak31Renderer::new);
	}
}
