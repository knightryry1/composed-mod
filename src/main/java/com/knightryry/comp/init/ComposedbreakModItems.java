
/*
 *	MCreator note: This file will be REGENERATED on each build.
 */
package com.knightryry.comp.init;

import net.minecraft.world.item.SpawnEggItem;
import net.minecraft.world.item.Item;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.core.Registry;

import com.knightryry.comp.ComposedbreakMod;

public class ComposedbreakModItems {
	public static Item COMPOSED_BREAK_31_SPAWN_EGG;

	public static void load() {
		COMPOSED_BREAK_31_SPAWN_EGG = Registry.register(Registry.ITEM, new ResourceLocation(ComposedbreakMod.MODID, "composed_break_31_spawn_egg"),
				new SpawnEggItem(ComposedbreakModEntities.COMPOSED_BREAK_31, -6749953, -6750004, new Item.Properties().tab(ComposedbreakModTabs.TAB_COMPOSED_BREAK)));
	}
}
