
/*
 *	MCreator note: This file will be REGENERATED on each build.
 */
package com.knightryry.comp.init;

import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.resources.ResourceLocation;

import net.fabricmc.fabric.api.client.itemgroup.FabricItemGroupBuilder;

public class ComposedbreakModTabs {
	public static CreativeModeTab TAB_COMPOSED_BREAK;

	public static void load() {
		TAB_COMPOSED_BREAK = FabricItemGroupBuilder.create(new ResourceLocation("composedbreak", "composed_break")).icon(() -> new ItemStack(ComposedbreakModItems.COMPOSED_BREAK_31_SPAWN_EGG)).build();
	}
}
