
/*
 *	MCreator note: This file will be REGENERATED on each build.
 */
package com.knightryry.comp.init;

import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.EntityDimensions;
import net.minecraft.world.entity.Entity;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.core.Registry;

import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricDefaultAttributeRegistry;

import com.knightryry.comp.entity.ComposedBreak31Entity;
import com.knightryry.comp.ComposedbreakMod;

public class ComposedbreakModEntities {
	public static EntityType<ComposedBreak31Entity> COMPOSED_BREAK_31;

	public static void load() {
		COMPOSED_BREAK_31 = Registry.register(Registry.ENTITY_TYPE, new ResourceLocation(ComposedbreakMod.MODID, "composed_break_31"),
				FabricEntityTypeBuilder.create(MobCategory.MONSTER, ComposedBreak31Entity::new).dimensions(new EntityDimensions(0.1f, 1.8f, true)).fireImmune().trackRangeBlocks(200).forceTrackedVelocityUpdates(true).trackedUpdateRate(3).build());
		ComposedBreak31Entity.init();
		FabricDefaultAttributeRegistry.register(COMPOSED_BREAK_31, ComposedBreak31Entity.createAttributes());
	}

	private static <T extends Entity> EntityType<T> createArrowEntityType(EntityType.EntityFactory<T> factory) {
		return FabricEntityTypeBuilder.create(MobCategory.MISC, factory).dimensions(EntityDimensions.fixed(0.5f, 0.5f)).trackRangeBlocks(1).trackedUpdateRate(64).build();
	}
}
