
/*
 *	MCreator note: This file will be REGENERATED on each build.
 */
package com.knightryry.comp.init;

import com.knightryry.comp.procedures.ComposedBreak31EntityDiesProcedure;

@SuppressWarnings("InstantiationOfUtilityClass")
public class ComposedbreakModProcedures {
	public static void load() {
		new ComposedBreak31EntityDiesProcedure();
	}
}
