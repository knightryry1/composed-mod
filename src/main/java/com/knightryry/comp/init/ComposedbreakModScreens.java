
/*
 *	MCreator note: This file will be REGENERATED on each build.
 */
package com.knightryry.comp.init;

import net.minecraft.client.gui.screens.MenuScreens;

import com.knightryry.comp.client.gui.InvScreen;

public class ComposedbreakModScreens {
	public static void load() {
		MenuScreens.register(ComposedbreakModMenus.INV, InvScreen::new);
	}
}
